<?php
/* @var $this CategoriesController */
/* @var $data Categories */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name_1')); ?>:</b>
	<?php echo CHtml::encode($data->name_1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name_2')); ?>:</b>
	<?php echo CHtml::encode($data->name_2); ?>
	<br />


</div>