<?php
/* @var $this PhrasesController */
/* @var $model Phrases */

$this->breadcrumbs=array(
	'Phrases'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Phrases', 'url'=>array('index')),
	array('label'=>'Create Phrases', 'url'=>array('create')),
	array('label'=>'View Phrases', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Phrases', 'url'=>array('admin')),
);
?>

<h1>Update Phrases <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>