<?php
/* @var $this PhrasesController */
/* @var $model Phrases */

$this->breadcrumbs=array(
	'Phrases'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Phrases', 'url'=>array('index')),
	array('label'=>'Create Phrases', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#phrases-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Phrases</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'phrases-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'categories_id' => array(
                    'name' => 'categories_id',
                    'value' => '$data->categories->name_1',
                    'headerHtmlOptions' => array('width' => 100),                    
                    'filter' => Categories::all(),
                ),
                'contexts_id' => array(
                    'name' => 'contexts_id',
                    'value' => '$data->contexts->title',
                    'headerHtmlOptions' => array('width' => 100),                    
                    'filter' => Contexts::all(),
                ),
                'meets_id' => array(
                    'name' => 'meets_id',
                    'value' => '$data->meets->title',
                    'headerHtmlOptions' => array('width' => 100),                    
                    'filter' => Meets::all(),
                ),
                'invites_id' => array(
                    'name' => 'invites_id',
                    'value' => '$data->invites->title',
                    'headerHtmlOptions' => array('width' => 100),                    
                    'filter' => Invites::all(),
                ),
		'phrase_1',
		'phrase_2',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
