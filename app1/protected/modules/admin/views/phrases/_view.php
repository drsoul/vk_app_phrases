<?php
/* @var $this PhrasesController */
/* @var $data Phrases */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('categories_id')); ?>:</b>
	<?php echo CHtml::encode($data->categories_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('phrase_1')); ?>:</b>
	<?php echo CHtml::encode($data->phrase_1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('phrase_2')); ?>:</b>
	<?php echo CHtml::encode($data->phrase_2); ?>
	<br />


</div>