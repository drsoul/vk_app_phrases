<?php
/* @var $this PhrasesController */
/* @var $model Phrases */

$this->breadcrumbs=array(
	'Phrases'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Phrases', 'url'=>array('index')),
	array('label'=>'Manage Phrases', 'url'=>array('admin')),
);
?>

<h1>Create Phrases</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>