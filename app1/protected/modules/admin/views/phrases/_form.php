<?php
/* @var $this PhrasesController */
/* @var $model Phrases */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'phrases-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'categories_id'); ?>
		<?php echo $form->dropDownList($model, 'categories_id', Categories::all(), array('empty'=>'')); ?>
		<?php echo $form->error($model,'categories_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'phrase_1'); ?>
		<?php echo $form->textArea($model,'phrase_1',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'phrase_1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'phrase_2'); ?>
		<?php echo $form->textArea($model,'phrase_2',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'phrase_2'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->