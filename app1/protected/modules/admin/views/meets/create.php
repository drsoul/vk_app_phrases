<?php
/* @var $this MeetsController */
/* @var $model Meets */

$this->breadcrumbs=array(
	'Meets'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Meets', 'url'=>array('index')),
	array('label'=>'Manage Meets', 'url'=>array('admin')),
);
?>

<h1>Create Meets</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>