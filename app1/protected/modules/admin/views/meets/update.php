<?php
/* @var $this MeetsController */
/* @var $model Meets */

$this->breadcrumbs=array(
	'Meets'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Meets', 'url'=>array('index')),
	array('label'=>'Create Meets', 'url'=>array('create')),
	array('label'=>'View Meets', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Meets', 'url'=>array('admin')),
);
?>

<h1>Update Meets <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>