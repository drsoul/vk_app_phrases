<?php
/* @var $this MeetsController */
/* @var $model Meets */

$this->breadcrumbs=array(
	'Meets'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List Meets', 'url'=>array('index')),
	array('label'=>'Create Meets', 'url'=>array('create')),
	array('label'=>'Update Meets', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Meets', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Meets', 'url'=>array('admin')),
);
?>

<h1>View Meets #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'title',
	),
)); ?>
