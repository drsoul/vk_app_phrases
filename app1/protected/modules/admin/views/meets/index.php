<?php
/* @var $this MeetsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Meets',
);

$this->menu=array(
	array('label'=>'Create Meets', 'url'=>array('create')),
	array('label'=>'Manage Meets', 'url'=>array('admin')),
);
?>

<h1>Meets</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
