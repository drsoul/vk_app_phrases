<?php
/* @var $this InvitesController */
/* @var $model Invites */

$this->breadcrumbs=array(
	'Invites'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List Invites', 'url'=>array('index')),
	array('label'=>'Create Invites', 'url'=>array('create')),
	array('label'=>'Update Invites', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Invites', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Invites', 'url'=>array('admin')),
);
?>

<h1>View Invites #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'title',
	),
)); ?>
