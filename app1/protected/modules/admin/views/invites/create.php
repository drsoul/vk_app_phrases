<?php
/* @var $this InvitesController */
/* @var $model Invites */

$this->breadcrumbs=array(
	'Invites'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Invites', 'url'=>array('index')),
	array('label'=>'Manage Invites', 'url'=>array('admin')),
);
?>

<h1>Create Invites</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>