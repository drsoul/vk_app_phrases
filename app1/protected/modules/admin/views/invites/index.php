<?php
/* @var $this InvitesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Invites',
);

$this->menu=array(
	array('label'=>'Create Invites', 'url'=>array('create')),
	array('label'=>'Manage Invites', 'url'=>array('admin')),
);
?>

<h1>Invites</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
