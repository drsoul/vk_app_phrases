<?php
/* @var $this InvitesController */
/* @var $model Invites */

$this->breadcrumbs=array(
	'Invites'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Invites', 'url'=>array('index')),
	array('label'=>'Create Invites', 'url'=>array('create')),
	array('label'=>'View Invites', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Invites', 'url'=>array('admin')),
);
?>

<h1>Update Invites <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>