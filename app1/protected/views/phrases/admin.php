<?php
/* @var $this PhrasesController */
/* @var $model Phrases */

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
        appeal_change();
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){   
        appeal_change();
	$('#phrases-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
"); 
 
?>




<script src="http://vk.com/js/api/xd_connection.js?2" type="text/javascript"></script>

<script type="text/javascript">
  VK.init(function() { 
        // API initialization succeeded 
        // Your code here         
     }, function() { 
        // API initialization failed 
        // Can reload page here        
   }, '5.8'); 
    
    function doIt(){        
        /*
        //рабочий вызов метода сбора фоток друзей
        VK.api("friends.get", {fields: "photo_100"}, function(data) { 
            // Действия с полученными данными 
            //alert(data.response[c].photo_medium);
            for(c=0;c<data.response.items.length;c++){
                $("body").append("<img src='"+data.response.items[c].photo_100+"'>");
            }
        });
        */   
        
    }
    
    $(document).ready(function(){
        doIt();    
    });
</script>    

<style>
    .hidden{
            visibility: hidden !important;
    }
    td.phrase_2{
            visibility: hidden;
    }
    td.phrase_1{
            visibility: hidden;
    }
    .visible{
            visibility: visible !important;
    }
</style>

    <script type="text/javascript">
        $(document).ready(function(){
            appeal_change();
            balans();
            init_orders();
        });
        
        function display_balans(balans){
            $('span#balans').text(balans);
        }
        
        function balans(){
             VK.api('storage.get', {key:'balans_first_stack'},function(data) {    

                if (data.response) {  
                    VK.api('storage.get', {key:'balans'},function(data) {
                        if (data.response) {
                            display_balans(data.response);
                        }else{
                            display_balans(0);
                        }
                    });
                }else{
                    balans = 5; //первая установка баланса
                    display_balans(balans);
                    set_vk_val('balans', balans);
                    set_vk_val('balans_first_stack', 1);//устанавливаем метку что
                    //мы уже выдали начальный бонус                    
                }
            });  
            
        }
        
        function balans_add(coins){
            coins = parseInt(coins);
            var balans_new = 0;
            VK.api('storage.get', {key:'balans'},function(data) {
                if (data.response) {
                    balans_new = parseInt(data.response) + coins;
                    display_balans(balans_new);
                    set_vk_val('balans', balans_new);                    
                }else{
                    balans_new = coins;
                    display_balans(balans_new);
                    set_vk_val('balans', balans_new); 
                }
            });            
        }
        
        function display_old_open_phrase_one(id_phrase){
            
            VK.api('storage.get', {key:id_phrase},function(data) {     

                if (data.response) {  
                    if (data.response === '1') { 
                        display_phrase_old(id_phrase);//видимость фразы
                        hide_display_button(id_phrase);                        
                    }                    
                }else{
                    //отображаем кнопку показать фразу
                    show_display_button(id_phrase);
                } 
            });
        }
        
        function display_phrase_old(id_phrase){
            var id_phrase_ar = id_phrase.split('_');
            var number_phrase = $("#select select").val();
            $('#phrase' + number_phrase + '_' + id_phrase_ar[1] ).addClass("visible");
        }
        
        function hide_display_button(id_phrase){
            var id_phrase_ar = id_phrase.split('_');
            $('#' + id_phrase_ar[1] ).addClass('hide');
        }
        
        function show_display_button(id_phrase){
            var id_phrase_ar = id_phrase.split('_');
            $('#' + id_phrase_ar[1] ).addClass('visible');
        }
        
        function display_old_open_phrase(){  
            var id_phrase = '';            
            
            $( 'td.phrase_1' ).each(function() {
                id_phrase = $( this ).attr('id');
                display_old_open_phrase_one(id_phrase);               
            });
        }
        
        function set_vk_val(name, value){
            VK.api('storage.set', {key: name ,value:value,global:0});
        }
        
        function appeal_change(){  
            
            if($("#select select").val() == 1){
                $('th.phrase_2, td.phrase_2').hide();
                $('th.phrase_1, td.phrase_1').show();
  
                //обработка выведенныех фраз на отображение
                display_old_open_phrase();
                
            }else if($("#select select").val() == 2){
                $('th.phrase_2, td.phrase_2').show();                 
                $('th.phrase_1, td.phrase_1').hide();
                
                //обработка выведенныех фраз на отображение
                display_old_open_phrase();
            }           
            
        }
        
        function no_money(){
            alert('У вас не хватает средств на балансе!');
        }       
        
        function display_phrase(e){
            VK.api('storage.get', {key:'balans'},function(data) {
                if (data.response) {
                    balans = parseInt(data.response ,10);                    
                    balans = balans - 1;
                    if(balans > -1){                        
                        
                        //номер нужного обращение (на вы, на ты)
                        var number_phrase = $("#select select").val();
                        
                        //отображает фразу
                        $('#phrase' + number_phrase + '_' + $(e).attr("href") ).addClass("visible");
                        display_balans(balans);
                        set_vk_val('balans', balans); 
                        set_vk_val('phrase1_' + $(e).attr("href"), 1);                                            
                        hide_display_button('phrase1_' + $(e).attr("href"));
                    }else{
                        no_money();                        
                    }                                       
                }else{
                    no_money();                    
                } 
            });
        }
        
        function order(votes) {
            var votes_int = parseInt(votes);
            var coins = 0;
            var params = {
                type: 'votes',
                votes: votes_int,
            };
            
            if(votes_int == 3){
                coins = 6;
            }else if(votes_int == 10){
                coins = 25;
            }else if(votes_int == 30){
                coins = 80;
            }
            $('input#tmp_s').val(coins);
            
            VK.callMethod('showOrderBox', params);
        }
        
        function init_orders(){
            var callbacksResults = document.getElementById('callbacks');

            VK.addCallback('onOrderSuccess', function(order_id) {
              var coins =  $('input#tmp_s').val();  
              callbacksResults.innerHTML += '<br />Баланс успешно пополнен на ' + coins + '<img src="images/coins.png" style="width:20px;">, заказ №: ' + order_id + '.';              
              balans_add(coins);
              $('#balans_add_menu_modal').modal('hide');
            });
            VK.addCallback('onOrderFail', function() {
              callbacksResults.innerHTML += '<br />Баланс не был пополнен.';
              $('#balans_add_menu_modal').modal('hide');
            });
            VK.addCallback('onOrderCancel', function() {
              callbacksResults.innerHTML += '<br />Пополнение отменено.';
            });
        }
    </script>

<a
 style="font-weight: bold;" href="javascript&#058;;"
 onclick="VK.callMethod('showSettingsBox', 256);">Добавить в левое меню</a> 
    &nbsp;
<a
 style="font-weight: bold;" href="javascript&#058;;"
 onclick="VK.External.showInviteBox();">Пригласить друзей</a> 
    
<? //var_dump($_GET); ?> 
    
<hr style="margin-top:20px;"/>

<span style="color:green; font-size:150%;">Ваш баланс: <span id="balans">Обрабатывается...</span> <img src="images/coins.png" style="width:20px;"></span> 
    &nbsp;
    <?php echo TbHtml::button('Пополнить', array(
    'style' => TbHtml::BUTTON_COLOR_PRIMARY,
    'color' => TbHtml::BUTTON_COLOR_SUCCESS,
    'size' => TbHtml::BUTTON_SIZE_SMALL,
    'data-toggle' => 'modal',
    'data-target' => '#balans_add_menu_modal',
)); ?>    
    <span id="callbacks"></span>

<br>За открытие фразы списывается 1 монетка.
<input type="hidden" id="tmp_s">
<style>
    .btn_buy_wrap{
        margin-top:10px;
    }
</style>
<?php $this->widget('bootstrap.widgets.TbModal', array(
    'id' => 'balans_add_menu_modal',
    'header' => 'Пополнение баланса',
    'content' => '
        <div class="row-fluid" style="text-align:center;">
            <div class="span4">
                <img src="images/coins.png" style="width:20px;"> 6 монет<br> за 3 голоса
                <div class="btn_buy_wrap"><button class="btn btn-success" onclick="order(3)">Купить</button></div>
            </div>
            <div class="span4">
                <img src="images/coins.png" style="width:20px;"> 25 монет<br> за 10 голосов
                <div class="btn_buy_wrap"><button class="btn btn-success" onclick="order(10)">Купить</button></div>
            </div>
            <div class="span4">
                <img src="images/coins.png" style="width:20px;"> 80 монет<br> за 30 голосов
                <div class="btn_buy_wrap"><button class="btn btn-success" onclick="order(30)">Купить</button></div>
            </div>
        </div>',
    'footer' => array(        
        TbHtml::button('Закрыть', array('data-dismiss' => 'modal')),
     ),
)); ?>
 


<hr style="margin-top:20px;"/>    

<form action="" method="get" id="select">    
    Обращение: 
    <select onchange="appeal_change()">
        <option value="1">"На ты"</option>
        <option value="2">"На вы"</option>
    </select>
</form>


<!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'phrases-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
        'afterAjaxUpdate'=>'function(){appeal_change();}',
	'columns'=>array(                
                'categories_id' => array(
                    'name' => 'categories_id',
                    'value' => '$data->categories->name_1',
                    'headerHtmlOptions' => array('width' => 10),
                    //'htmlOptions'=>array('style'=>'visibility: hidden;'),
                    'filter' => Categories::all(),
                ),
                'contexts_id' => array(
                    'name' => 'contexts_id',
                    'value' => '$data->contexts->title',
                    'headerHtmlOptions' => array('width' => 10), 
                    //'htmlOptions'=>array('style'=>'visibility: hidden;'),
                    'filter' => Contexts::all(),
                ),
                'meets_id' => array(
                    'name' => 'meets_id',
                    'value' => '$data->meets->title',
                    'headerHtmlOptions' => array('width' => 10),
                    //'htmlOptions'=>array('style'=>'visibility: hidden;'),
                    'filter' => Meets::all(),
                ),               
                'invites_id' => array(
                    'name' => 'invites_id',
                    'value' => '$data->invites->title',
                    'headerHtmlOptions' => array('width' => 10),
                    //'htmlOptions'=>array('style'=>'visibility: hidden;'),
                    'filter' => Invites::all(),
                ),
		/*'id' => array(
                    'name' => 'id',
                    'headerHtmlOptions' => array('width' => 30),
                    'filter' => false,
                ),*/
		'phrase_1' => array(
                    'class'=>'DataColumn',
                    'name' => 'phrase_1',
                    'value' => '$data->phrase_1',
                    'headerHtmlOptions' => array('class' => 'phrase_1'),
                    'evaluateHtmlOptions'=>true,          
                    'htmlOptions'=>array('id'=>'"phrase1_{$data->id}"', 'class'=>'phrase_1',),
                    'filter' => false,
                ),
		'phrase_2' => array(
                    'class'=>'DataColumn',
                    'name' => 'phrase_2',
                    'value' => '$data->phrase_2',
                    'headerHtmlOptions' => array('class' => 'phrase_2'),
                    'evaluateHtmlOptions'=>true,   
                    'htmlOptions'=>array('id'=> '"phrase2_{$data->id}"', 'class'=>'phrase_2'),
                    'filter' => false,
                ),
                array(            // display a column with "view", "update" and "delete" buttons
                    'class'=>'ButtonColumn',
                    'template'=>'{display}',
                    'evaluateHtmlOptions'=>true,                    
                    'buttons'=>array
                    (
                        'display' => array
                        (
                            'label'=>'Показать',     //Text label of the button.
                            //'url'=>'#',       //A PHP expression for generating the URL of the button.
                            //'imageUrl'=>'',  //Image URL of the button.
                            'options'=>array(
                                'title'=>'Показать фразу',
                                'id'=> '$data->id',
                                'style'=>'visibility: hidden;',
                                ), //HTML options for the button tag.
                            'url' => '$data->id',
                            'click'=>'function() { display_phrase(this); return false;}',     //A JS function to be invoked when the button is clicked.
                            'visible'=>'true',   //A PHP expression for determining whether the button is visible.                            
                        )
                    ),
                ),

	),
)); ?>


