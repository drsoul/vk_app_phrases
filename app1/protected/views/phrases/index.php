<?php
/* @var $this PhrasesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Phrases',
);

$this->menu=array(
	array('label'=>'Create Phrases', 'url'=>array('create')),
	array('label'=>'Manage Phrases', 'url'=>array('admin')),
);
?>

<h1>Phrases</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
