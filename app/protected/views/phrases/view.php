<?php
/* @var $this PhrasesController */
/* @var $model Phrases */

$this->breadcrumbs=array(
	'Phrases'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Phrases', 'url'=>array('index')),
	array('label'=>'Create Phrases', 'url'=>array('create')),
	array('label'=>'Update Phrases', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Phrases', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Phrases', 'url'=>array('admin')),
);
?>

<h1>View Phrases #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'categories_id',
		'phrase_1',
		'phrase_2',
	),
)); ?>
