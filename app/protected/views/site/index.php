
<?php
/* @var $this PagesController */
/* @var $model Pages */


$this->menu=array(	
	array('label'=>'Создать страницу', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#phrases-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
//var_dump($model->search());
?>
</div><!-- search-form -->
<div class="form">


<?php   $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'phrases-grid',
	'dataProvider'=>array($model->search()->getData()),
	'filter'=>$model->search(),
	'columns'=>array(
            'id' => array(
                'name' => 'id',
                'headerHtmlOptions' => array('width' => 30),
            ),
         
            'categories_id' => array(
                'name' => 'categories_id',
                'value' => '$data->categories_id',
                'filter' => Categories::all(),
            ),
            		
            'phrase_1' => array(
                'name' => 'phrase_1',
                'value' => '$data->phrase_1',
                'filter' => true,
            ),
            		
            'phrase_2' => array(
                'name' => 'phrase_2',
                'value' => '$data->phrase_2',
                'filter' => true,
            ),

		array(
			'class'=>'CButtonColumn',
                        'viewButtonUrl' => 'CHtml::normalizeUrl(array("/phrases/view", "id" => $data->id))',
		),
	),
));  ?>