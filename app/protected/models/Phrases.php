<?php

/**
 * This is the model class for table "phrases".
 *
 * The followings are the available columns in table 'phrases':
 * @property integer $id
 * @property integer $categories_id
 * @property string $phrase_1
 * @property string $phrase_2
 */
class Phrases extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
    
        public static function phrasesByParams($deal = 1, $cat = 1){
            
                $criteria = new CDbCriteria;
                //$criteria->params(array(':deal'=>$deal, ':cat'=>$cat));
                //if($deal == 2){                     
                    //$criteria->select = array('id', 'phrase_2', 'categories_id');
                //}else{
                    $criteria->select = array('id', 'phrase_1', 'phrase_2', 'categories_id');
                //}
                //$criteria->compare('phrases', $deal);
                $criteria->addCondition('categories_id = :categories_id');
                $criteria->params = array(':categories_id' => $cat);
                //$criteria->compare('categories_id', $cat);
                
 
                //$criteria->order = 'id DESC';               
            
                
                $dataProvider = new CActiveDataProvider('Phrases', array('criteria'=>$criteria));
                //var_dump($dataProvider->getData());
                //return new CActiveDataProvider('Phrases', array('criteria'=>$criteria));
                return $dataProvider;
        }
        
	public function tableName()
	{
		return 'phrases';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
                        array('categories_id', 'required'),
			array('id, categories_id', 'numerical', 'integerOnly'=>true),
			array('phrase_1, phrase_2', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, categories_id, phrase_1, phrase_2', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                        'categories' => array(self::BELONGS_TO, 'Categories', 'categories_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'categories_id' => 'Категория',
			'phrase_1' => 'Фраза с обращением - "на ты"',
			'phrase_2' => 'Фраза с обращением - "на вы"',
                        'deal' => 'Обращение',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()//$deal = 1)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('categories_id',$this->categories_id,true);
                /*
                if($deal == 2){                     
                    $criteria->select = array('id', 'phrase_2', 'categories_id');
                }else{
                    $criteria->select = array('id', 'phrase_1', 'categories_id');
                }
                */
		$criteria->compare('phrase_1',$this->phrase_1,true);
		$criteria->compare('phrase_2',$this->phrase_2,true);
                
                //$criteria->compare('deal',$this->deal,array(1=>"На ты", 2=> "На вы"));
                $CActiveDataProvider = new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		)); 
                /*
                var_dump($CActiveDataProvider->getData());
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
                */
                return $CActiveDataProvider;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Phrases the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
